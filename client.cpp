#include "mycp.h"
#include <cstdint>
#include <sys/sem.h>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <getopt.h>
#include <signal.h>

using namespace std;

int shm_id = -500;
int sem_id = -500;
char* shm_buf = NULL;

void _free_sem_shm(void) {
	if (shm_id != -500) {
		if (shmctl(shm_id, IPC_RMID, NULL) == -1) {
			perror("shmctl erorr");
			exit(EXIT_FAILURE);
		}
		if (shmdt(shm_buf) == -1) {
			perror("shmdt error");
			exit(EXIT_FAILURE);
		}
	}
	if (sem_id != -500) {
		if (semctl(sem_id, 0, IPC_RMID) == -1) {
			perror("Server: semctl error");
			exit(EXIT_FAILURE);
		}
	}
}

void sign_term(int SIGNO) {
	exit(EXIT_FAILURE);
}


int main(int argc, char* argv[]) {
	atexit(_free_sem_shm);
	signal(SIGINT, sign_term);
	int opt;
	while (true) {
		opt = getopt(argc, argv, ":h?");
		if (opt == -1) {
			break;
		}
		switch (opt) {
			case 'h':
			{
				cout << "cp-client [OPTION] DEST\n\nCopy file from shared memory block to DEST. Use cp-server SOURCE first to copy from shared memory to DEST.\nOPTION:\n\t-h\tShow this help\nVladimir Pervushin 2017\n";
				break;
			}
			case '?':
			{
				cerr << "Try \'cp-server -h\' to get help\n";
				break;
			}
		}
	}
	if (argc == 1) {
		cerr << "Too few agruments...\n";
		exit(EXIT_FAILURE);
	} else if (argc > 2) {
		cerr << "Too many agruments...\n";
		exit(EXIT_FAILURE);
	}

	int shm_id = shmget(ftok(".", 1996), BLOCK_SIZE * Shm_iterator::N_BLOCKS + 9, 0); 
	if (shm_id == -1) {
		cerr<<"Client: server is not running\n";
		exit(EXIT_FAILURE);
	}
	char* shm_buf = (char*)shmat(shm_id, NULL, 0);
	int sem_id = semget(ftok(".", 1997), NUM_SEMS, 0);
	if (sem_id == -1) {
		perror("Client: semget error");
		exit(EXIT_FAILURE);
	}
	ofstream ofs(argv[optind], ios::binary);
	uint64_t* p = (uint64_t*)(shm_buf + NUM_PCKGS);
	uint64_t n_pckg = *p;
	//cout << "Copying into " << argv[optind] << ". Found " << n_pckg << " packages\n";
	Shm_iterator iter;
	for (uint64_t i = 0; i < n_pckg; i++) {
		if (semop(sem_id, lock_full, 1) == -1) {
			perror("Client: lock_full error");
			exit(EXIT_FAILURE);
		}
		ofs.write(shm_buf + iter.i*BLOCK_SIZE, BLOCK_SIZE);
		++iter;
		if (semop(sem_id, post_empty, 1) == -1) {
			perror("Client: post_empty error");
			exit(EXIT_FAILURE);
		}
	}
	char *p_last = shm_buf + NUM_LAST_PCKG;
	if (*p_last != 0) {
		if (semop(sem_id, lock_full, 1) == -1) {
			perror("Client: lock_full error");
			exit(EXIT_FAILURE);
		}
		ofs.write(shm_buf + iter.i*BLOCK_SIZE, *p_last);
		++iter;
		if (semop(sem_id, post_empty, 1) == -1) {
			perror("Client: post_empty error");
			exit(EXIT_FAILURE);
		}		
	}
	if (shmdt(shm_buf) == -1) {
		perror("shmdt error");
		exit(EXIT_FAILURE);
	}
	ofs.close();
	if (semop(sem_id, post_barr, 1) == -1) {
		perror("Client: post_barr error");
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}