TARGETS = server client
SOURCES = $(wildcard *.cpp)
OBJECTS=$(SOURCES:%.cpp=%.o)
CXXFLAGS += -g -std=c++11

all: $(TARGETS)

server: server.o mycp.o
	$(CXX) -o server $(LDFLAGS) server.o mycp.o $(LOADLIBES) $(LDLIBS)

client: client.o mycp.o
	$(CXX) -o client $(LDFLAGS) client.o mycp.o $(LOADLIBES) $(LDLIBS)

$(OBJECTS): $(SOURCES)

clean:
	$(RM) $(OBJECTS) $(TARGET)

.PHONY: all clean