#include "mycp.h"	

size_t Shm_iterator::N_BLOCKS = 8;

Shm_iterator Shm_iterator::operator++() {
		i++;
		if (i == N_BLOCKS) {
			i = 0;
		}
		return *this;
}

