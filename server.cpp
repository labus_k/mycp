#include "mycp.h"
#include <cstdint>
#include <sys/sem.h>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

using namespace std;

int shm_id = -500;
int sem_id = -500;
char* shm_buf = NULL;

void _free_sem_shm(void) {
	if (shm_id != -500) {
		if (shmctl(shm_id, IPC_RMID, NULL) == -1) {
			perror("shmctl erorr");
			exit(EXIT_FAILURE);
		}
		if (shmdt(shm_buf) == -1) {
			perror("shmdt error");
			exit(EXIT_FAILURE);
		}
	}
	if (sem_id != -500) {
		if (semctl(sem_id, 0, IPC_RMID) == -1) {
			perror("Server: semctl error");
			exit(EXIT_FAILURE);
		}
	}
}

void sign_term(int SIGNO) {
	exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
	atexit(_free_sem_shm);
	signal(SIGINT, sign_term);
	int opt;
	while (true) {
		opt = getopt(argc, argv, ":h?");
		if (opt == -1) {
			break;
		}
		switch (opt) {
			case 'h':
			{
				cout << "cp-server [OPTION] SOURCE\n\nCopy SOURCE to shared memory block. Use cp-client to copy from shared memory to in_file.\nOPTION:\n\t-h\tShow this help\nVladimir Pervushin 2017\n";
				break;
			}
			case '?':
			{
				cerr << "Try \'cp-server -h\' to get help\n";
				break;
			}

		}
	}
	if (argc == 1) {
		cerr << "Too few arguments...\n";
		exit(EXIT_FAILURE);
	} else if (argc > 2) {
		cerr << "Too many arguments...\n";
		exit(EXIT_FAILURE);
	}
	shm_id = shmget(ftok(".", 1996), BLOCK_SIZE * Shm_iterator::N_BLOCKS + 9, IPC_CREAT); 
	if (shm_id == -1) {
		perror("Server: shmget error");
		exit(EXIT_FAILURE);
	}
	shm_buf = (char*)shmat(shm_id, NULL, 0);
	if (shm_buf == (void*)-1) {
		perror("Server: shmat error");
		exit(EXIT_FAILURE);
	}
	sem_id = semget(ftok(".", 1997), NUM_SEMS, IPC_CREAT);
	if (sem_id == -1) {
		perror("Server: semget error");
		exit(EXIT_FAILURE);
	}
	
	if (semctl(sem_id, 0, SETALL, arg_array) == -1) {
		perror("Server: semctl erro");
		exit(EXIT_FAILURE);
	}
	
	ifstream in_file(argv[optind], ios::binary | ios::ate);
	if (in_file.is_open() == false) {
		cerr << "Error open file\n";
		exit(EXIT_FAILURE);
	}
	uint64_t fsize = in_file.tellg();
	in_file.seekg(0, in_file.beg);
	uint64_t pckg_size = fsize / BLOCK_SIZE;
	char* p_last = shm_buf + NUM_LAST_PCKG;
	if (fsize % BLOCK_SIZE != 0) {
		*p_last = (char)(fsize - pckg_size*BLOCK_SIZE);
	} else {
		*p_last = 0;
	}
	uint64_t *p = (uint64_t*)(shm_buf + NUM_PCKGS);
	*p = pckg_size;
	//cout << "Num of packages: " << *p << endl;
	Shm_iterator iter;
	char fbuf[BLOCK_SIZE];
	for (uint64_t i = 0; i < pckg_size; i++) {
		if(semop(sem_id, lock_empty, 1) == -1) {
			perror("Server: lock_empty erorr");
			exit(EXIT_FAILURE);
		}
		in_file.read(shm_buf + iter.i*BLOCK_SIZE, BLOCK_SIZE);
		++iter;
		if(semop(sem_id, post_full, 1) == -1) {
			perror("Server: post_full error");
			exit(EXIT_FAILURE);
		}
	}
	if (*p_last != 0) {
		if(semop(sem_id, lock_empty, 1) == -1) {
			perror("Server: lock_empty erorr");
			exit(EXIT_FAILURE);
		}
		in_file.read(shm_buf + iter.i*BLOCK_SIZE, *p_last);
		++iter;
		if(semop(sem_id, post_full, 1) == -1) {
			perror("Server: post_full error");
			exit(EXIT_FAILURE);
		}
	}
	in_file.close();

	if (semop(sem_id, wait_barr, 1) == -1) {
		perror("Server: wait_barr error");
		exit(EXIT_FAILURE);
	}

/*	if (shmctl(shm_id, IPC_RMID, NULL) == -1) {
		perror("shmctl erorr");
		exit(EXIT_FAILURE);
	}
	if (shmdt(shm_buf) == -1) {
		perror("shmdt error");
		exit(EXIT_FAILURE);
	}
	if (semctl(sem_id, 0, IPC_RMID) == -1) {
		perror("Server: semctl error");
		exit(EXIT_FAILURE);
	}
	*/
}
