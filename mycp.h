#ifndef MYCP_H
#define MYCP_H

//#include <sys/types.h>
#include <cstdint>
#include <sys/sem.h>
#define BLOCK_SIZE 128
#define NUM_SEMS 3
#define NUM_PCKGS 1024
#define NUM_LAST_PCKG 1032

enum n_sems {FULL_BLOCK = 0, EMPTY_BLOCK, BARR_WAIT};

static struct sembuf lock_full[1] = { 
	{FULL_BLOCK, -1, 0}
};

static struct sembuf post_full[1] = {
	{FULL_BLOCK, 1, 0}
};

static struct sembuf lock_empty[1] = { 
	{EMPTY_BLOCK, -1, 0}
};

static struct sembuf post_empty[1] = {
	{EMPTY_BLOCK, 1, 0}
};

static struct sembuf post_barr[1] = {
	{BARR_WAIT, -1, 0}
};

static struct sembuf wait_barr[1] = {
	{BARR_WAIT, 0, 0}
};

static unsigned short arg_array[] = {0, 8, 1};

struct Shm_iterator {
	uint64_t i;
	static uint64_t N_BLOCKS;
	Shm_iterator() : i(0) {}
	Shm_iterator operator++();
};




#endif